package com.seop.personaltrainingmanager.service;

import com.seop.personaltrainingmanager.entity.PersonalTrainingCustomer;
import com.seop.personaltrainingmanager.model.CustomerInfoUpdateRequest;
import com.seop.personaltrainingmanager.model.CustomerItem;
import com.seop.personaltrainingmanager.model.CustomerRequest;
import com.seop.personaltrainingmanager.model.CustomerWeightUpdateRequest;
import com.seop.personaltrainingmanager.repository.PersonalTrainingRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final PersonalTrainingRepository personalTrainingRepository;

    public void setCustomer(CustomerRequest request) {
        PersonalTrainingCustomer addData = new PersonalTrainingCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setHeight(request.getHeight());
        addData.setWeight(request.getWeight());
        addData.setDateFirst(LocalDateTime.now());

        personalTrainingRepository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<PersonalTrainingCustomer> originData = personalTrainingRepository.findAll();
        List<CustomerItem> result = new LinkedList<>();

        for (PersonalTrainingCustomer item :originData) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setHeight(item.getHeight());
            addItem.setWeight(item.getWeight());
            addItem.setDateFirst(item.getDateFirst());
            addItem.setDateLast(item.getDateLast());

            float heightDouble = (item.getHeight() * item.getHeight()) / 1000;
            double bmi = item.getWeight() / heightDouble;

            addItem.setBmi(bmi);

            String bmiResult = "";
            if (bmi <= 18.5) {
                bmiResult = "저체중";
            } else if (bmi <= 22.9) {
                bmiResult = "정상";
            } else if (bmi <= 24.9) {
                bmiResult = "과체중";
            } else {
                bmiResult = "비만";
            }
            addItem.setBmiResult(bmiResult);
            result.add(addItem);
        }
        return result;
    }

    public void putCustomerInfo(long id, CustomerInfoUpdateRequest request) {
        PersonalTrainingCustomer originData = personalTrainingRepository.findById(id).orElseThrow();
        originData.setCustomerName(request.getCustomerName());
        originData.setCustomerPhone(request.getCustomerPhone());

        personalTrainingRepository.save(originData);
    }

    public void putCustomerWeight(long id, CustomerWeightUpdateRequest request) {
        PersonalTrainingCustomer originData = personalTrainingRepository.findById(id).orElseThrow();
        originData.setWeight(request.getWeight());

        personalTrainingRepository.save(originData);
    }

    public void putCustomerVisit(long id) {
        PersonalTrainingCustomer originData = personalTrainingRepository.findById(id).orElseThrow();
        originData.setDateLast(LocalDateTime.now());

        personalTrainingRepository.save(originData);
    }
}
