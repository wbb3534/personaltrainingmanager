package com.seop.personaltrainingmanager.repository;

import com.seop.personaltrainingmanager.entity.PersonalTrainingCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalTrainingRepository extends JpaRepository<PersonalTrainingCustomer, Long> {
}
