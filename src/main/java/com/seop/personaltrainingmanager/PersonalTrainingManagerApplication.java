package com.seop.personaltrainingmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalTrainingManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalTrainingManagerApplication.class, args);
    }

}
