package com.seop.personaltrainingmanager.controller;

import com.seop.personaltrainingmanager.model.CustomerInfoUpdateRequest;
import com.seop.personaltrainingmanager.model.CustomerItem;
import com.seop.personaltrainingmanager.model.CustomerRequest;
import com.seop.personaltrainingmanager.model.CustomerWeightUpdateRequest;
import com.seop.personaltrainingmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;
    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();
        return result;
    }

    @PutMapping("/info/id/{id}")
    public String putCustomerInfo(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdateRequest request) {
        customerService.putCustomerInfo(id, request);
        return "ok";
    }

    @PutMapping("/weight/id/{id}")
    public String putCustomerWeight(@PathVariable long id, @RequestBody @Valid CustomerWeightUpdateRequest request) {
        customerService.putCustomerWeight(id, request);
        return "ok";
    }

    @PutMapping("/visit/id/{id}")
    public String putCustomerVisit(@PathVariable long id) {
        customerService.putCustomerVisit(id);
        return "ok";
    }
}
